<?php

/**
 * builds the form to display on the media mover admin page
 */
function s3_api_admin_form() {
  $form['s3_api'] = array(
      '#type' => 'fieldset',
      '#title' => t("S3 configuration"),
      '#description' => t('Settings for the S3 module. You can see if your connection is functional by using the !link.',
      array('!link' => l(t('S3 test'), 'admin/build/s3_ap1/test'))),
      '#element_validate' => array('s3_api_admin_validate'),
  );

  $form['s3_api']['s3_api_bucket'] = array(
      '#type' => 'textfield',
      '#title' => t('S3 Bucket'),
      '#default_value' => variable_get('s3_api_bucket', str_replace(' ', '_', variable_get('site_name', t('My Site')))),
      '#description' => t("Name of the S3 bucket, note this has to be unique. This can be overridden by a configuration, but this is the default value."),
  );

  $form['s3_api']['s3_api_default_perm'] = array(
      '#type' => 'select',
      '#options' => s3_api_file_perms(),
      '#title' => t('S3 Default Permissions'),
      '#default_value' => variable_get('s3_api_default_perm', ACL_PUBLIC_READ),
      '#description' => t('Default permissions on files and buckets created on S3'),
  );

  $form['s3_api']['s3_api_default_server_url'] = array(
      '#type' => 'textfield',
      '#title' => t('S3 URL'),
      '#default_value' => variable_get('s3_api_default_server_url', "https://s3.amazonaws.com/"),
      '#description' => t('URL to send to amazon. You probably do not need to change this.'),
  );

  $form['s3_api']['s3_api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('S3 Key'),
      '#default_value' => variable_get('s3_api_key', ""),
      '#description' => t("S3 key."),
  );

  $form['s3_api']['s3_api_skey'] = array(
      '#type' => 'textfield',
      '#title' => t('S3 Secret Key'),
      '#default_value' => variable_get('s3_api_skey', ""),
      '#description' => t("S3 secret key."),
  );

// Other settings
  $form['s3_local'] = array(
      '#type' => 'fieldset',
      '#title' => t("S3 Api Local Settings"),
      '#description' => t('Set up some local tasks for the S3 Api library.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE
  );
  $form['s3_local']['delete_local_file'] = array(
      '#type' => 'checkbox',
      '#title' => t('Delete local file'),
      '#default_value' => variable_get('delete_local_file', false),
      '#description' => t('Delete local file once upload is completed.'),
  );
// lib settings
  $form['s3_lib'] = array(
      '#type' => 'fieldset',
      '#title' => t("S3 Api Library Settings (Optional)"),
      '#description' => t('Make sure you have active the correct version of the S3 Api Lib.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE
  );

  $form['s3_lib']['s3_api_lib_version'] = array(
      '#type' => 'select',
      '#options' => array('3.8' => '3.8', '3.9' => '3.9', '4.0' => '4.0'),
      '#title' => t('S3 Api Lib version'),
      '#default_value' => variable_get('s3_api_lib_version', '3.8'),
  );
  return system_settings_form($form);
}


/**
 * Validate the connection specified in the admin settings
 * @param $element
 * @param $form_element
 */
function s3_api_admin_validate($element, &$form_state) {
  // only validate if we have both values
  if ($form_state['values']['s3_api_key'] && $form_state['values']['s3_api_skey']) {
    // we need to verify the parameters being passed in
    if (! s3_api_get_instance($form_state['values']['s3_api_key'], $form_state['values']['s3_api_skey'])) {
      form_error($element, '');
      return;
    }

    // does this bucket exist?
    if (! s3_api_bucket_exists($form_state['values']['s3_api_bucket'])) {
      // try to create the bucket
      if (! s3_api_bucket_create($form_state['values']['s3_api_bucket'],  $form_state['values']['s3_api_default_perm'])) {
        form_error($element, t('Could not create your bucket on the Amazon servers. You need to choose a different name'));
      }
    }
  }
}
